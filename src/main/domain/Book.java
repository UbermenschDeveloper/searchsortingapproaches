package main.domain;

import java.util.Objects;

/**
 * TODO Write proper documentation and don't forget about testing & logging
 */

public class Book implements Comparable<Book>{

    private int uniqueNumber;
    private String title;

    public Book (String title, int uniqueNumber){
        this.uniqueNumber = uniqueNumber;
        this.title = title;
    }
    public int getUniqueNumber() {
        return uniqueNumber;
    }

    public void setUniqueNumber(int uniqueNumber) {
        this.uniqueNumber = uniqueNumber;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    @Override
    public int compareTo(Book o) {
        if(getUniqueNumber() == o.getUniqueNumber()){
            return getTitle().compareTo(o.getTitle());
        } else {
            return getUniqueNumber() > o.getUniqueNumber() ? 1 : -1;
        }
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Book book = (Book) o;

        if (getUniqueNumber() != book.getUniqueNumber()) return false;
        return getTitle() != null ? getTitle().equals(book.getTitle()) : book.getTitle() == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (getUniqueNumber() ^ (getUniqueNumber() >>> 32));
        result = 31 * result + (getTitle() != null ? getTitle().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Book{" +
                "uniqueNumber=" + uniqueNumber +
                '}';
    }
}
