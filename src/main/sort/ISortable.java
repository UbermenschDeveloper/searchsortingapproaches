package main.sort;

import main.domain.Book;

import java.util.List;

public interface ISortable {
    void sort(List<Book> library);
    int swapsCount();
}