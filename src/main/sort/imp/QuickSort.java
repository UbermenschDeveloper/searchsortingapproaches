package main.sort.imp;

import main.domain.Book;
import main.sort.ISortable;

import java.util.List;


public class QuickSort implements ISortable {

    private int swaps;
    @Override
    public void sort(List<Book> library) {
        sort(library, 0, library.size()-1);
    }

    @Override
    public int swapsCount() {
        return swaps;
    }

    private void sort(List<Book> library, int left, int right) {
        int i = left, j = right;
        Book tmp;
        Book pivot = library.get((left + right) / 2);

        while (i <= j) {
            while (library.get(i).compareTo(pivot) < 0)
                i++;
            while (library.get(j).compareTo(pivot) > 0)
                j--;
            if (i <= j) {
                tmp = library.get(i);
                library.set(i, library.get(j));
                library.set(j, tmp);
                ++swaps;
                i++;
                j--;
            }
        };

        if (left < i - 1)
            sort(library, left, i - 1);
        if (i < right)
            sort(library, i, right);
    }
}
