package main.sort.imp;

import main.domain.Book;
import main.sort.ISortable;

import java.util.ArrayList;
import java.util.List;


public class MergeSort implements ISortable {

    private int swaps;
    @Override
    public void sort(List<Book> library) {
        List<Book> left = new ArrayList<>();
        List<Book> right = new ArrayList<>();
        int center;

        if (library.size() == 1) {
            return;
        } else {
            center = library.size()/2;
            // copy the left half of whole into the left.
            for (int i=0; i<center; i++) {
                left.add(library.get(i));
            }

            //copy the right half of whole into the new arraylist.
            for (int i=center; i<library.size(); i++) {
                right.add(library.get(i));
            }

            // sort the left and right halves of the arraylist.
            sort(left);
            sort(right);

            // Merge the results back together.
            sort(left, right, library);
        }
    }

    @Override
    public int swapsCount() {
        return swaps;
    }

    private void sort(List<Book> left, List<Book> right, List<Book> whole) {
        int leftIndex = 0;
        int rightIndex = 0;
        int wholeIndex = 0;

        // As long as neither the left nor the right ArrayList has
        // been used up, keep taking the smaller of left.get(leftIndex)
        // or right.get(rightIndex) and adding it at both.get(bothIndex).
        while (leftIndex < left.size() && rightIndex < right.size()) {
            if ( (left.get(leftIndex).compareTo(right.get(rightIndex))) < 0) {
                whole.set(wholeIndex, left.get(leftIndex));
                leftIndex++;
                ++swaps;
            } else {
                whole.set(wholeIndex, right.get(rightIndex));
                rightIndex++;
                ++swaps;
            }
            wholeIndex++;
        }

        List<Book> rest;
        int restIndex;
        if (leftIndex >= left.size()) {
            // The left ArrayList has been use up...
            rest = right;
            restIndex = rightIndex;
        } else {
            // The right ArrayList has been used up...
            rest = left;
            restIndex = leftIndex;
        }

        // Copy the rest of whichever ArrayList (left or right) was not used up.
        for (int i=restIndex; i<rest.size(); i++) {
            whole.set(wholeIndex, rest.get(i));
            wholeIndex++;
        }
    }
}
