package main.sort.imp;

import main.domain.Book;
import main.sort.ISortable;

import java.util.List;


public class SelectionSort implements ISortable {

    private int swaps;

    @Override
    public void sort(List<Book> library) {
        int min;
        Book temp;
        for(int i = 0; i <  library.size()-1; ++i){
            min = i;
            for(int j = i + 1; j < library.size(); ++j){
                if(library.get(j).compareTo(library.get(min)) < 0){
                    min = j;
                }
            }
            temp = library.get(min);
            library.set(min, library.get(i));
            library.set(i, temp);
            ++swaps;
        }
    }

    @Override
    public int swapsCount() {
        return swaps;
    }
}
