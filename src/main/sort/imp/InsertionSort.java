package main.sort.imp;

import main.domain.Book;
import main.sort.ISortable;

import java.util.List;


public class InsertionSort implements ISortable {

    private int swaps;
    @Override
    public void sort(List<Book> library) {
        for(int i = 1; i < library.size(); ++i){
            for(int j  = i; j > 0; --j){
                if(library.get(j).compareTo(library.get(j-1)) < 0){
                    Book temp = library.get(j);
                    library.set(j, library.get(j-1));
                    library.set(j-1, temp);
                    ++swaps;
                }
            }
        }
    }

    @Override
    public int swapsCount() {
        return swaps;
    }
}
