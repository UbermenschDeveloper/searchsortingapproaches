package main.sort.imp;

import main.domain.Book;
import main.sort.ISortable;

import java.util.List;


public class BubbleSort implements ISortable {

    private int swaps = 0;
    @Override
    public void sort(List<Book> library) {
        for(int i = 0; i < library.size(); ++i){
            for(int j = 1; j < library.size() - i; ++j){
                if(library.get(j).compareTo(library.get(j-1)) < 0){
                    Book temp = library.get(j-1);
                    library.set(j-1, library.get(j));
                    library.set(j, temp);
                    ++swaps;
                }
            }
        }
    }

    @Override
    public int swapsCount() {
        return swaps;
    }
}
