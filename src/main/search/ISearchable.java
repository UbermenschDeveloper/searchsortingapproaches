package main.search;

import main.domain.Book;

import java.util.List;

public interface ISearchable {
    Book search(List<Book> library, Book key);
}
