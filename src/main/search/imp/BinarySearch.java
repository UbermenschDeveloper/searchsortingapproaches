package main.search.imp;

import main.domain.Book;
import main.search.ISearchable;

import java.util.List;

public class BinarySearch implements ISearchable {
    @Override
    public Book search(List<Book> library, Book key) {
        if(library.size()==1 && !library.get(0).equals(key)){
            throw new IllegalArgumentException();
        }
        int m = library.size()/2;
        if(library.get(m).equals(key)){
            return library.get(m);
        }else if(key.compareTo(library.get(m))>0){
            return search(library.subList(m, library.size()), key);
        } else {
            return search(library.subList(0, m), key);
        }
    }
}
