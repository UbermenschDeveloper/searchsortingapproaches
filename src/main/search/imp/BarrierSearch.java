package main.search.imp;

import main.domain.Book;
import main.search.ISearchable;

import java.util.List;

public class BarrierSearch implements ISearchable {
    @Override
    public Book search(List<Book> library, Book key) {
        int length = library.size();
        if(library.get(length-1).equals(key)) return library.get(length-1);
        else {
            library.set(length-1, key);
            int position = 0;
            while (!library.get(position).equals(key)){
                position++;
            }
            if(position==length-1) throw new IllegalArgumentException();
            else return library.get(position);
        }
    }
}
