package main.search.imp;

import main.domain.Book;
import main.search.ISearchable;

import java.util.List;

public class InterpolationSearch implements ISearchable {
    @Override
    public Book search(List<Book> library, Book key) {
        int length = library.size();
        int left = 0;
        int right = length-1;

        while (library.get(left).compareTo(key) < 0 && key.compareTo(library.get(right)) < 0){
            int mid = (int) (left + ((long) (key.getUniqueNumber() - library.get(left).getUniqueNumber()) * (right-left))
                    / (library.get(right).getUniqueNumber() - library.get(left).getUniqueNumber()));
            if(library.get(mid).compareTo(key)<0){
                left = mid + 1;
            } else if (key.compareTo(library.get(mid))<0){
                right = mid -1;
            } else {
                return library.get(mid);
            }
        }

        if(library.get(left).equals(key)) return library.get(left);
        else if(library.get(right).equals(key)) return library.get(right);
        else throw new IllegalArgumentException();
    }
}
