package main.search.imp;

import main.domain.Book;
import main.search.ISearchable;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;


public class RandomSearch implements ISearchable {
    @Override
    public Book search(List<Book> library, Book key) {
        int length = library.size();
        boolean [] checked = new boolean [length];
        int checkedCount = 0;
        while(checkedCount<length-1) {
            int randomIndex = ThreadLocalRandom.current().nextInt(0, length);
            if(!checked[randomIndex]) {
                if(library.get(randomIndex).equals(key)){
                    return library.get(randomIndex);
                }else {
                    checked[randomIndex] = true;
                    ++checkedCount;
                }
            }
        }
        throw new IllegalArgumentException();
    }
}
