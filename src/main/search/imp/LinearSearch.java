package main.search.imp;

import main.domain.Book;
import main.search.ISearchable;

import java.util.List;

public class LinearSearch implements ISearchable {
    @Override
    public Book search(List<Book> library, Book key) {
        for(int i = 0; i < library.size(); ++i){
            if(library.get(i).equals(key)){
                return library.get(i);
            }
        }
        throw new IllegalArgumentException();
    }
}
