package main.search.imp;

import main.domain.Book;
import main.search.ISearchable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TrieSearch implements ISearchable {
    private ArrayList<HashMap<Character, Integer>> graph;
    private ArrayList<Integer> indexContainer;

    @Override
    public Book search(List<Book> library, Book key) {
        buildGraph(library);
        int index = 0;
        char[] keyArray = String.valueOf(key.getUniqueNumber()).toCharArray();
        for(Character character : keyArray){
            Integer newIndex = graph.get(index).get(character);
            if(newIndex != null){
                index = newIndex;
                continue;
            }
            throw new IllegalArgumentException();
        }
        return library.get(indexContainer.get(index));
    }

    private void buildGraph(List<Book> books){
        graph = new ArrayList<>();
        graph.add(new HashMap<>());
        indexContainer = new ArrayList<>();
        indexContainer.add(-1);
        int counter = 0;
        int i = 0;
        for(Book book : books){
            int index = 0;
            char[] keyArray = String.valueOf(book.getUniqueNumber()).toCharArray();
            for(Character character : keyArray){
                Integer newIndex = graph.get(index).get(character);
                if(newIndex != null){
                    index = newIndex;
                    continue;
                }
                counter++;
                graph.get(index).put(character, counter);
                graph.add(new HashMap<>());
                indexContainer.add(-1);
                index = counter;
            }
            indexContainer.set(index, i);
            i++;
        }
    }
}
