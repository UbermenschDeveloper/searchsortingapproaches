package test.java.sort;


import main.domain.Book;
import main.sort.*;

import main.sort.imp.*;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;
import test.java.analyse.Analyzer;
import test.java.analyse.imp.SortAnalyzer;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SortTest {

    private ISortable sortHandler;
    private List<Book> library;
    private List<Book> sortedLibrary;
    private Analyzer analyzer = SortAnalyzer.getInstance();

    @Before
    public void runBeforeEveryTest() throws Exception{
        //getting data from .json file
        Object obj = new JSONParser().parse(new FileReader(
                "C:\\Users\\asus\\IdeaProjects\\SearchAndSortingApproaches\\src\\main\\resources\\sort-test.json"));
        JSONArray jsonArray = (JSONArray) obj;
        //configuring library and shuffling it
        library = new ArrayList<>();
        for(Object currentObj : jsonArray){
            JSONObject jsonObject = (JSONObject) currentObj;
            Book book = new Book(jsonObject.get("title").toString(), Integer.valueOf(jsonObject.get("uniqueNumber").toString()));
            library.add(book);
        }
        Collections.shuffle(library);
        //configuring another library and sorting it
        sortedLibrary = new ArrayList<>();
        for(Object currentObj : jsonArray){
            JSONObject jsonObject = (JSONObject) currentObj;
            Book book = new Book(jsonObject.get("title").toString(), Integer.valueOf(jsonObject.get("uniqueNumber").toString()));
            sortedLibrary.add(book);
        }
        Collections.sort(sortedLibrary);
    }

    @org.junit.Test
    public void bubbleSort() throws Exception {
        sortHandler = new BubbleSort();
        long startTime = System.currentTimeMillis();
        sortHandler.sort(library);
        long stopTime = System.currentTimeMillis();
        Assert.assertEquals(sortedLibrary, library);
        analyzer.writeSpentTime(stopTime-startTime, "bubble sort");
        int swaps = sortHandler.swapsCount();
        analyzer.writeSwapsCount(swaps, "bubble sort");
    }

    @org.junit.Test
    public void insertionSort() throws Exception {
        sortHandler = new InsertionSort();
        long startTime = System.currentTimeMillis();
        sortHandler.sort(library);
        long stopTime = System.currentTimeMillis();
        Assert.assertEquals(sortedLibrary, library);
        analyzer.writeSpentTime(stopTime-startTime, "insertion sort");
        int swaps = sortHandler.swapsCount();
        analyzer.writeSwapsCount(swaps, "insertion sort");
    }

    @org.junit.Test
    public void selectionSort() throws Exception {
        sortHandler = new SelectionSort();
        long startTime = System.currentTimeMillis();
        sortHandler.sort(library);
        long stopTime = System.currentTimeMillis();
        Assert.assertEquals(sortedLibrary, library);
        analyzer.writeSpentTime(stopTime-startTime, "selection sort");
        int swaps = sortHandler.swapsCount();
        analyzer.writeSwapsCount(swaps, "selection sort");
    }

    @org.junit.Test
    public void quickSort() throws Exception {
        sortHandler = new QuickSort();
        long startTime = System.currentTimeMillis();
        sortHandler.sort(library);
        long stopTime = System.currentTimeMillis();
        Assert.assertEquals(sortedLibrary, library);
        analyzer.writeSpentTime(stopTime-startTime, "quick sort");
        int swaps = sortHandler.swapsCount();
        analyzer.writeSwapsCount(swaps, "quick sort");
    }

    @org.junit.Test
    public void mergeSort() throws Exception {
        sortHandler = new MergeSort();
        long startTime = System.currentTimeMillis();
        sortHandler.sort(library);
        long stopTime = System.currentTimeMillis();
        Assert.assertEquals(sortedLibrary, library);
        analyzer.writeSpentTime(stopTime-startTime, "merge sort");
        int swaps = sortHandler.swapsCount();
        analyzer.writeSwapsCount(swaps, "merge sort");
    }
}