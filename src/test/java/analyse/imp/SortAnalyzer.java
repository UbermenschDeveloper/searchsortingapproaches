package test.java.analyse.imp;

import test.java.analyse.Analyzer;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/**
 * TODO Write proper documentation and don't forget about testing & logging
 */

public class SortAnalyzer implements Analyzer {

    private final static String FILE_NAME = "sort-statistics.txt";
    private static SortAnalyzer instance;


    public static SortAnalyzer getInstance(){
        if(instance==null){
            instance = new SortAnalyzer();
        }
        return instance;
    }

    private SortAnalyzer() {
        writeHeader();
    }

    @Override
    public void writeHeader() {
        appendString("Sort statistics\n\n");
    }

    @Override
    public void writeSpentTime(long time, String operationName) {
        String text = "It took " + String.valueOf(time) + "ms for " + operationName + ".\n";
        appendString(text);
    }

    @Override
    public void writeSwapsCount(int swaps, String operationName) {
        String text = "It took " + String.valueOf(swaps) + " swaps for " + operationName + ".\n";
        appendString(text);
    }

    private void appendString(String content){
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(FILE_NAME, true))) {
            bw.write(content);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
