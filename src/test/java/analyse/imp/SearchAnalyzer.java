package test.java.analyse.imp;

import test.java.analyse.Analyzer;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/**
 * TODO Write proper documentation and don't forget about testing & logging
 */

public class SearchAnalyzer implements Analyzer {

    private final static String FILE_NAME = "search-statistics.txt";
    private static SearchAnalyzer instance;

    private SearchAnalyzer(){
        writeHeader();
    }

    public static SearchAnalyzer getInstance(){
        if(instance==null){
            instance = new SearchAnalyzer();
        }
        return instance;
    }

    @Override
    public void writeHeader() {
        appendString("Search statistics\n\n");
    }

    @Override
    public void writeSpentTime(long time, String operationName) {
        String text = "It took " + String.valueOf(time) + "ms for " + operationName + ".\n";
        appendString(text);
    }

    @Override
    public void writeSwapsCount(int swaps, String operationName) {

    }

    private void appendString(String content){
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(FILE_NAME, true))) {
            bw.write(content);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
