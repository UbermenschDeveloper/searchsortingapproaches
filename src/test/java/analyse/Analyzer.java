package test.java.analyse;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/**
 * TODO Write proper documentation and don't forget about testing & logging
 */

public interface Analyzer {
    void writeHeader();
    void writeSpentTime(long time, String operationName);
    void writeSwapsCount(int swaps, String operationName);
}
