package test.java.search;

import main.domain.Book;
import main.search.ISearchable;
import main.search.imp.*;

import main.sort.ISortable;
import main.sort.imp.QuickSort;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import test.java.analyse.Analyzer;
import test.java.analyse.imp.SearchAnalyzer;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SearchTest {

    private ISearchable searchHandler;
    private ISortable sortHandler;
    private List<Book> library;
    private List<Book> sortedLibrary;
    private Analyzer analyzer = SearchAnalyzer.getInstance();

    @Before
    public void configDummies() throws IOException, ParseException {
        //getting data from .json file
        Object obj = new JSONParser().parse(new FileReader(
                "C:\\Users\\asus\\IdeaProjects\\SearchAndSortingApproaches\\src\\main\\resources\\search-test.json"));
        JSONArray jsonArray = (JSONArray) obj;
        //configuring library and shuffling it
        library = new ArrayList<>();
        for(Object currentObj : jsonArray){
            JSONObject jsonObject = (JSONObject) currentObj;
            Book book = new Book(jsonObject.get("title").toString(), Integer.valueOf(jsonObject.get("uniqueNumber").toString()));
            library.add(book);
        }
        Collections.shuffle(library);
        //configuring another library and sorting it
        sortedLibrary = new ArrayList<>();
        for(Object currentObj : jsonArray){
            JSONObject jsonObject = (JSONObject) currentObj;
            Book book = new Book(jsonObject.get("title").toString(), Integer.valueOf(jsonObject.get("uniqueNumber").toString()));
            sortedLibrary.add(book);
        }
        Collections.sort(sortedLibrary);
        sortHandler = new QuickSort();
    }


    @Test
    public void binarySearchShouldFindKeyFirstElement() throws Exception {
        searchHandler = new BinarySearch();
        Book key = sortedLibrary.get(0);
        Book result = searchHandler.search(sortedLibrary, key);
        Assert.assertEquals(key, result);
    }

    @Test
    public void binarySearchShouldFindKeyMiddleElement() throws Exception{
        searchHandler = new BinarySearch();
        Book key = sortedLibrary.get(sortedLibrary.size()/2);
        Book result = searchHandler.search(sortedLibrary, key);
        Assert.assertEquals(key, result);

    }

    @Test
    public void binarySearchShouldFindKeyLastElement() throws Exception{
        searchHandler = new BinarySearch();
        Book key = sortedLibrary.get(sortedLibrary.size()-1);
        Book result = searchHandler.search(sortedLibrary, key);
        Assert.assertEquals(key, result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void binarySearchFailSearchingNotExistingElement() throws Exception{
        searchHandler = new BinarySearch();
        Book key = new Book("title", -1);
        Book result = searchHandler.search(library, key);
        Assert.assertEquals(key, result);
    }

    @Test
    public void barrierSearchShouldFindKeyFirstElement() throws Exception {
        searchHandler = new BarrierSearch();
        Book key = sortedLibrary.get(0);
        Book result = searchHandler.search(sortedLibrary, key);
        Assert.assertEquals(key, result);
    }

    @Test
    public void barrierSearchShouldFindKeyMiddleElement() throws Exception{
        searchHandler = new BarrierSearch();
        Book key = library.get(library.size()/2);
        Book result = searchHandler.search(library, key);
        Assert.assertEquals(key, result);
    }

    @Test
    public void barrierSearchShouldFindKeyLastElement() throws Exception{
        searchHandler = new BarrierSearch();
        Book key = library.get(library.size()-1);
        Book result = searchHandler.search(library, key);
        Assert.assertEquals(key, result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void barrierSearchFailSearchingNotExistingElement() throws Exception{
        searchHandler = new BarrierSearch();
        Book key = new Book("title", -1);
        Book result = searchHandler.search(library, key);
        Assert.assertEquals(key, result);
    }

    @Test
    public void linearSearchShouldFindKeyFirstElement() throws Exception {
        searchHandler = new LinearSearch();
        Book key = library.get(0);
        Book result = searchHandler.search(library, key);
        Assert.assertEquals(key, result);
    }

    @Test
    public void linearSearchShouldFindKeyMiddleElement() throws Exception{
        searchHandler = new LinearSearch();
        Book key = library.get(library.size()/2);
        Book result = searchHandler.search(library, key);
        Assert.assertEquals(key, result);
    }

    @Test
    public void linearSearchShouldFindKeyLastElement() throws Exception{
        searchHandler = new LinearSearch();
        Book key = library.get(library.size()-1);
        Book result = searchHandler.search(library, key);
        Assert.assertEquals(key, result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void linearSearchFailSearchingNotExistingElement() throws Exception{
        searchHandler = new LinearSearch();
        Book key = new Book("title", -1);
        Book result = searchHandler.search(library, key);
        Assert.assertEquals(key, result);
    }

    @Test
    public void randomSearchShouldFindKeyFirstElement() throws Exception {
        searchHandler = new RandomSearch();
        Book key = library.get(0);
        Book result = searchHandler.search(library, key);
        Assert.assertEquals(key, result);
    }

    @Test
    public void randomSearchShouldFindKeyMiddleElement() throws Exception{
        searchHandler = new RandomSearch();
        Book key = library.get(library.size()/2);
        Book result = searchHandler.search(library, key);
        Assert.assertEquals(key, result);
    }

    @Test
    public void randomSearchShouldFindKeyLastElement() throws Exception{
        searchHandler = new RandomSearch();
        Book key = library.get(library.size()-1);
        Book result = searchHandler.search(library, key);
        Assert.assertEquals(key, result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void randomSearchFailSearchingNotExistingElement() throws Exception{
        searchHandler = new RandomSearch();
        Book key = new Book("title", -1);
        Book result = searchHandler.search(library, key);
        Assert.assertEquals(key, result);
    }

    @Test
    public void interpolationSearchShouldFindKeyFirstElement() throws Exception {
        searchHandler = new InterpolationSearch();
        Book key = sortedLibrary.get(0);
        Book result = searchHandler.search(sortedLibrary, key);
        Assert.assertEquals(key, result);
    }

    @Test
    public void interpolationSearchShouldFindKeyMiddleElement() throws Exception{
        searchHandler = new InterpolationSearch();
        Book key = sortedLibrary.get(sortedLibrary.size()/2);
        Book result = searchHandler.search(sortedLibrary, key);
        Assert.assertEquals(key, result);
    }

    @Test
    public void interpolationSearchShouldFindKeyLastElement() throws Exception{
        searchHandler = new InterpolationSearch();
        Book key = sortedLibrary.get(sortedLibrary.size()-1);
        Book result = searchHandler.search(sortedLibrary, key);
        Assert.assertEquals(key, result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void interpolationSearchFailSearchingNotExistingElement() throws Exception{
        searchHandler = new InterpolationSearch();
        Book key = new Book("title", -1);
        Book result = searchHandler.search(sortedLibrary, key);
        Assert.assertEquals(key, result);
    }

    @Test
    public void trieSearchShouldFindKeyFirstElement() throws Exception {
        searchHandler = new TrieSearch();
        Book key = sortedLibrary.get(0);
        Book result = searchHandler.search(sortedLibrary, key);
        Assert.assertEquals(key, result);
    }

    @Test
    public void trieSearchShouldFindKeyMiddleElement() throws Exception{
        searchHandler = new TrieSearch();
        Book key = sortedLibrary.get(sortedLibrary.size()/2);
        Book result = searchHandler.search(sortedLibrary, key);
        Assert.assertEquals(key, result);
    }

    @Test
    public void trieSearchShouldFindKeyLastElement() throws Exception{
        searchHandler = new TrieSearch();
        Book key = sortedLibrary.get(sortedLibrary.size()-1);
        Book result = searchHandler.search(sortedLibrary, key);
        Assert.assertEquals(key, result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void trieSearchFailSearchingNotExistingElement() throws Exception{
        searchHandler = new TrieSearch();
        Book key = new Book("title", -1);
        Book result = searchHandler.search(sortedLibrary, key);
        Assert.assertEquals(key, result);
    }

}